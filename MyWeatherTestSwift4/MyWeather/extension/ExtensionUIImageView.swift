//
//  ExtensionUIImageView.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 31/12/2017.
//  Copyright © 2017 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func applyMotionEffect(magnitude: Float) {
        
        let xMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = -magnitude
        xMotion.maximumRelativeValue = magnitude
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = -magnitude
        yMotion.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [xMotion, yMotion]
        
        self.addMotionEffect(group)
    }
}
