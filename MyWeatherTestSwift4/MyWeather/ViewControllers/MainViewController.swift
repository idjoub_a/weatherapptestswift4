//
//  MainViewController.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 26/12/2017.
//  Copyright © 2017 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import PullUpController
import CoreLocation
import SDWebImage

class MainViewController: UIViewController {
    
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var list: UICollectionView!

    private var prevision: Prevision? = nil
    private var myCellID = "DayDetailWeatherCollectionViewCell"
    
    private let locationManager = CLLocationManager()
    
    private var latitude: Double? = 0
    private var longitude: Double? = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        let nibCell = UINib(nibName: self.myCellID, bundle: nil)
        list.register(nibCell, forCellWithReuseIdentifier: self.myCellID)
        
        background.applyMotionEffect(magnitude: 20)
        
        setCLocation()

        addPullUpController()
    }

    private func addPullUpController() {
        guard let pullUpController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
            else { return }
        
        addPullUpController(pullUpController)
    }
    
     private func callAPI() {
        let url = Networking().buildUrl(latitude!, longitude!)
        Networking().apiCalling(url, objectType: Prevision.self) { model in
            self.prevision = model
            print("My model is \(String(describing: self.prevision))")
            self.printData()
        }
    }
    
    private func printData() {
        DispatchQueue.main.async {
            guard let prevision = self.prevision,
                let todayPrevision = prevision.forecastDays.first else {
                    print("weather not received")
                    return
            }
            
            self.city.text = "\(prevision.city.name), \(prevision.city.country)"
            self.temperature.text = "\(String(format: "%.f", todayPrevision.temperature.day.rounded()))°C"
            
            self.list.reloadData()
        }
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let prevision = self.prevision else {
            return 0
        }
        return prevision.forecastDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = 0
        return UIEdgeInsetsMake(CGFloat(inset), CGFloat(inset), CGFloat(inset), CGFloat(inset))
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: myCellID, for: indexPath) as! DayDetailWeatherCollectionViewCell

        guard let prevision = self.prevision else {
            return cell
        }
        
        
        cell.day.text = getDate(unixdate: prevision.forecastDays[indexPath.row].date)
        
        cell.icon.sd_setImage(with: URL(string:"http://openweathermap.org/img/w/\(prevision.forecastDays[indexPath.row].weather.first!.icon).png"), completed: nil)

        cell.temperature.text = "\(String(format: "%.f", prevision.forecastDays[indexPath.row].temperature.day))°C"

        return cell
    }
}


extension MainViewController: CLLocationManagerDelegate{
    
    func setCLocation() {
        locationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.distanceFilter = 5000.0
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first, (self.latitude == 0 && self.longitude == 0) {
            self.latitude = location.coordinate.latitude
            self.longitude = location.coordinate.longitude
            callAPI()
            print(location.coordinate)
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled",
                                                message: "In order to get your meteo we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}


/*
func test(input: String, completion: (_ result: String) -> Void) {
    let str: String = "we finished!"
    completion(str)
}

test(input: "commands") { result in
    print("got back: \(result)")
}
*/
