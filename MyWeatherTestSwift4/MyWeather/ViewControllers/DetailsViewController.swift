//
//  DetailsViewController.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 31/12/2017.
//  Copyright © 2017 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import PullUpController

class DetailsViewController: PullUpController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.cornerRadius = 10
    }

    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 130)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
