//
//  Networking.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 31/12/2017.
//  Copyright © 2017 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation
/*
enum BaseUrl: String {
    case websiteDesc = "http://api.letsbuildthatapp.com/jsondecodable/website_description"
    case weather = "http://api.wunderground.com/api/2fbdcd7d75e80b92/forecast/q/BH/Manama.json"
    case weatherBis = "http://api.openweathermap.org/data/2.5/forecast/daily?units=metric&lat=26.209&lon=50.576&cnt=7&lang=en&APPID=edef4985c1e5777dee5ccc8f3ee062e0"
}
*/
final class Networking {
    
    func buildUrl(_ latitude: Double, _ longitude: Double) -> String {
        let strLatitude = "\(latitude)"
        let strLongitude = "\(longitude)"
        
        let url = "http://api.openweathermap.org/data/2.5/forecast/daily?units=metric&lat=\(strLatitude)&lon=\(strLongitude)&cnt=7&lang=en&APPID=edef4985c1e5777dee5ccc8f3ee062e0"
        
        return url
    }
    
    func apiCalling<T>(_ url: String, objectType: T.Type, completion: @escaping (_ model: T) -> Void) where T : Decodable {

        //let stringUrl = urlType.rawValue
        
        guard let url = URL(string: url) else {
            print("=>Error URL")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                print("=>Error 1")
                return
            }
            
            guard let response = response as? HTTPURLResponse, 200..<300 ~= response.statusCode else {
                print("=>Error 2")
                return
            }

            guard let data = data else {
                print("=>Error 3")
                return
            }
            
            do {
                let result = try JSONDecoder().decode(objectType.self, from: data)
                print("decoded SUCCESS")
                completion(result)
                //print(websiteDescription.courses.count)
            }
            catch let error{
                print("=>Error 4")
            }
        }.resume()
    }
}
